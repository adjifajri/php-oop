<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");  
echo "Nama Animal : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 2
echo "Cold Blooded : ";
var_dump($sheep->cold_blooded); // false

echo "<br>";
echo "<br>";

$frog = new Frog("frog");
echo "Nama Animal : " . $frog->name ."<br>";
echo "Jumlah Kaki : " . $frog->legs ."<br>";
echo $frog->jump() ."<br>";

echo "<br>";

$ape = new Ape("ape");
echo "Nama Animal : " . $ape->name ."<br>";
echo "Jumlah Kaki : " . $ape->legs ."<br>";
echo $ape->yell() ."<br>";